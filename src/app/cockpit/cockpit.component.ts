import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {

  @Output() createdServer = new EventEmitter < { serverName: string, newServerContent: string } > ();
  @Output() createdBlueprint = new EventEmitter < { blueprintName: string, newBlueprintContent: string } > ();
  @ViewChild('inputNewServerContent') inputNewServerContent: ElementRef;
  constructor() {}

  ngOnInit(): void {}

  onAddServer(serverName: HTMLInputElement) {
    this.createdServer.emit({
      serverName: serverName.value,
      newServerContent: this.inputNewServerContent.nativeElement.value
    });
  }

  onAddBlueprint(serverName: HTMLInputElement) {
    this.createdBlueprint.emit({
      blueprintName: serverName.value,
      newBlueprintContent: this.inputNewServerContent.nativeElement.value
    });
  }

}
